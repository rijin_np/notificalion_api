# Create your views here.
from rest_framework.views import APIView

from core.response import exception_response
from notifications.push_notifications import multi_notify


class SendNotification(APIView):
    def post(self, request):
        try:
            multi_notify(users=request.POST.get('users'),
                         message=request.POST.get('message'),
                         notification_type='TEST_NOTIFY',
                         icon='',
                         title=request.POST.get('title'))
        except Exception as e:
            exception_response(e)
