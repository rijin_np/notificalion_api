from django.conf.urls import url

from notifications.views import SendNotification

urlpatterns = [
    url(r'^send_notifications/', SendNotification.as_view())
]
