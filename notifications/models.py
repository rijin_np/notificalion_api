from django.db import models

# Create your models here.
from user.models import User

NOTIFICATION_TYPE = (
    ('TEST_NOTIFY', 'Test Notification'),
)


class NotificationIcon(models.Model):
    label = models.CharField('label', max_length=25, unique=True)
    icon = models.ImageField(upload_to='notification_icons')


class Notification(models.Model):
    client = models.ForeignKey(User, on_delete=models.CASCADE, related_name='notification')
    type = models.CharField(
        max_length=25, default='TEST_NOTIFY', choices=NOTIFICATION_TYPE)
    title = models.CharField(max_length=50, blank=True, null=True)
    message = models.CharField(max_length=250, blank=True, null=True)
    icon = models.ForeignKey(NotificationIcon,
                             on_delete=models.CASCADE)
