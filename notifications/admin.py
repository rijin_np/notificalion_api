from django.contrib import admin

# Register your models here.
from notifications.models import Notification, NotificationIcon

admin.site.register(Notification)
admin.site.register(NotificationIcon)