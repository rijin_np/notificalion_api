import json

import requests

from notifications.models import NotificationIcon
from user.models import User

serverToken = "AAAAZzKfUss:APA91bGQpL07W940N2Y7nfTPGyPl-_CDI6ZY3dqIAqFXPzahjejEI_XnMBn25i7-jVtSRv_lqACc5sCUXpXll34fIpGXSTXzdEkV3BQVdLK0bYGIZIxRvgoIES6kFNC2MiPBl98SPQQj"


def send_notification(device_id, data, title, message_body):
    """
    method for firebase push notifications
    :param device_id: device id
    :param data: extra data ex: type and id
    :param title: notification title
    :param message_body: message string
    :return: message response
    """
    headers = {
        "Content-Type": "application/json",
        "Authorization": "key=" + serverToken
    }
    body = {
        'notification': {
            'title': title,
            'body': message_body,
        },
        'to': device_id,
        'priority': 'high',
        'data': data,
    }
    return requests.post(
        "https://fcm.googleapis.com/fcm/send",
        headers=headers,
        data=json.dumps(body),
    )


def notify(user, actor, verb, action='', notification_type='', description='', icon='', title='', details=''):
    """
    create a single user notification
    :param details: complete details of object.
    :param title: notification title
    :param icon: notification icon
    :param user: user
    :param actor: The object that performed the activity.
    :param verb: The verb phrase that identifies the action of the activity
    :param action: An object of any type. (Optional)
    :param notification_type: The object to which the activity was performed. (Optional)
    :param description: An string. (Optional)
    :return: notification object
    """

    if notification_type:
        if NotificationIcon.objects.get_by_filter(label=notification_type).exists():
            icon = NotificationIcon.objects.get(label=notification_type)
        else:
            icon = NotificationIcon.objects.get(label='TEST')

    n = user.notification.create(
        actor=actor, verb=verb, action=action, type=notification_type, description=description, icon=icon)
    if user.fcm_token:
        msg_body = actor + ' ' + verb
        notification_data = {
            'type': notification_type,
            'id': action,
            'data': details
        }
        send_notification(data=notification_data,
                          title=title,
                          message_body=msg_body,
                          device_id=user.fcm_token)

    return n


def multi_notify(users, message, notification_type='', icon='', title='', details=''):
    """
    create multiple users notification
    :param message:
    :param details: complete details of object.
    :param title:
    :param icon: notification icon
    :param users: users
    :param notification_type: The object to which the activity was performed. (Optional)
    :return: notification object
    """
    if notification_type:
        if NotificationIcon.objects.filter(label=notification_type).exists():
            icon = NotificationIcon.objects.get(label=notification_type)
        else:
            icon = NotificationIcon.objects.get(label='TEST')

    if users:
        for u in User.objects.filter(pk__in=users):
            u.notification.create(
                message=message, type=notification_type, icon=icon)
            if u.fcm_token:
                msg_body = message
                notification_data = {
                    'type': notification_type,
                    'data': details
                }
                send_notification(data=notification_data,
                                  title=title,
                                  message_body=msg_body,
                                  device_id=u.fcm_token)
