from rest_framework import status
from rest_framework.response import Response


def exception_response(e):
    return Response(
        {
            'status': 403,
            'error': e.__str__()
        },
        status=status.HTTP_403_FORBIDDEN
    )
